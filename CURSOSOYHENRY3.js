// No cambies los nombres de las funciones.

// Devuelve el primer elemento de un array
// Tu código:

function devolverPrimerElemento(array) {
    array = ["a", "b", "c", "d"];
    return array[0];
}
devolverPrimerElemento();
console.log(devolverPrimerElemento());

// Devuelve el último elemento de un array
// Tu código:

function devolverUltimoElemento(array) {
    array = ["a", "b", "c", "d"];
    return array[3];
}
devolverUltimoElemento();
console.log(devolverUltimoElemento());

// Devuelve el largo de un array
// Tu código:

function obtenerLargoDelArray(array) {
    array = ["a", "b", "c", "d"];
    return array.length;
}
obtenerLargoDelArray();
console.log(obtenerLargoDelArray());

// "array" debe ser una matriz de enteros (int/integers)
// Aumenta cada entero por 1
// y devuelve el array
// Tu código:

function incrementarPorUno(array) {
    array = [1, 2, 3, 4, 5];
    for (i = 0; i < array.length; i++) {
        console.log(array[i] + 1);
    }
}

incrementarPorUno();

// Añade el "elemento" al final del array
// y devuelve el array
// Tu código:

function agregarItemAlFinalDelArray(array, elemento) {
    array = ["Soda Stero", "Virus", "Spinetta"];
    elemento = array.push("Sumo");
    return array;
}
agregarItemAlFinalDelArray();
console.log(agregarItemAlFinalDelArray());

// Añade el "elemento" al comienzo del array
// y devuelve el array
// Pista: usa el método `.unshift`
// Tu código:

function agregarItemAlComienzoDelArray(array, elemento) {
    array = ["Soda Stero", "Virus", "Spinetta"];
    elemento = array.unshift("Pappo");
    return array;
}
agregarItemAlComienzoDelArray();
console.log(agregarItemAlComienzoDelArray());

// "palabras" es un array de strings/cadenas
// Devuelve un string donde todas las palabras estén concatenadas
// con espacios entre cada palabra
// Ejemplo: ['Hello', 'world!'] -> 'Hello world!'
// Tu código:

function dePalabrasAFrase(palabras) {
    palabras = "Hello " + "Word!";
    return palabras;
}
dePalabrasAFrase();
console.log(dePalabrasAFrase());

// Comprueba si el elemento existe dentro de "array"
// Devuelve "true" si está, o "false" si no está
// Tu código:

let array = ["Soda Stero", "Virus", "Los Piojos"];
function arrayContiene(array, elemento) {
    return array.some(function (arrayelemento) {
        return elemento === arrayelemento;
    });
}

console.log(arrayContiene(array, "Luca Prodan"));
console.log(arrayContiene(array, "Virus"));

// "numeros" debe ser un arreglo de enteros (int/integers)
// Suma todos los enteros y devuelve el valor
// Tu código:

function agregarNumeros(numeros) {
    var numeros = [6, 5, 8, 4, 7];
    let suma = 0;
    numeros.forEach(function (numero) {
        suma += numero;
    });
    console.log(suma);
}
agregarNumeros();

// "resultadosTest" debe ser una matriz de enteros (int/integers)
// Itera (en un bucle) los elementos del array, calcula y devuelve el promedio de puntajes
// Tu código:

function promedioResultadosTest(resultadosTest) {
    resultadosTest = [20, 40, 50, 60, 80, 100];
    let sum = resultadosTest.reduce((previous, current) => (current += previous));
    let avg = sum / resultadosTest.length;
    return avg;
}
promedioResultadosTest();
console.log(promedioResultadosTest());

// "numeros" debe ser una matriz de enteros (int/integers)
// Devuelve el número más grande
// Tu código:

function numeroMasGrande(numeros) {
    numero = [1, 2, 3, 4, 5, 6];
    let max = Math.max(...numero);
    return max;
}

numeroMasGrande();
console.log(numeroMasGrande());

// Usa la palabra clave `arguments` para multiplicar todos los argumentos y devolver el producto
// Si no se pasan argumentos devuelve 0. Si se pasa un argumento, simplemente devuélvelo
// Escribe tu código aquí:

function multiplicarArgumentos() {
    let x = 9;
    let y = 6;
    let argument = y * x;
    if (argument != 0) {
        return argument;
    } else return 0;
}
multiplicarArgumentos();
console.log(multiplicarArgumentos());

//Realiza una función que retorne la cantidad de los elementos del arreglo cuyo valor es mayor a 18.
//Escribe tu código aquí

function cuentoElementos(arreglo) {
    arreglo = [3, 15, 20, 30];
    const maximo = arreglo.find((element) => element > 18);
    return maximo;
}
cuentoElementos();
console.log(cuentoElementos());

//Suponga que los días de la semana se codifican como 1 = Domingo, 2 = Lunes y así sucesivamente.
//Realiza una función que dado el número del día de la semana, retorne: Es fin de semana
//si el día corresponde a Sábado o Domingo y 'Es dia Laboral' en caso contrario.
//Escribe tu código aquí
function diaDeLaSemana(numeroDeDia) {
    numeroDeDia = 7;
    if (numeroDeDia == 1) {
        return "Es Domingo Fin de Semana";
    } else if (numeroDeDia == 2) {
        return "Es Lunes Dia Laboral";
    } else if (numeroDeDia == 3) {
        return "Es Martes Dia Laboral";
    } else if (numeroDeDia == 4) {
        return "Es Miercoles Dia Laboral";
    } else if (numeroDeDia == 5) {
        return "Es Jueves Dia Laboral";
    } else if (numeroDeDia == 6) {
        return "Es Viernes Dia Laboral";
    } else numeroDeDia == 7;
    return console.log("Sabado fin de semana");
}
diaDeLaSemana();

//Desarrolle una función que recibe como parámetro un número entero n. Debe retornar true si el entero
//inicia con 9 y false en otro caso.
//Escribe tu código aquí

function empiezaConNueve(n) {
    n = 9;
    let nueve = 9;
    let noventa = 90;
    let novecientos = 900;
    let mil = 1000;
    if (n >= nueve && n < 10) {
        return console.log("True");
    } else if (n >= noventa && n <= 99) {
        return console.log("true");
    } else if (n >= novecientos && n < 1000) {
        return console.log("true");
    } else return console.log("False");
}
empiezaConNueve();

//Escriba la función todosIguales, que indique si todos los elementos de un arreglo son iguales:
//retornar true, caso contrario retornar false.
//Escribe tu código aquí

function todosIguales(arreglo) {
    arreglo = [1, "siete", 6, "ocho"];
    let arr = arreglo.indexOf(6);
    if (arr !== -1) {
        return "True";
    } else return "False";
}
todosIguales();
console.log(todosIguales());

//Dado un array que contiene algunos meses del año desordenados, recorrer el array buscando los meses de
// "Enero", "Marzo" y "Noviembre", guardarlo en nuevo array y retornarlo.
//Si alguno de los meses no está, devolver: "No se encontraron los meses pedidos"
// Tu código:

function mesesDelAño(array) {
    array = ["Agosto", "Enero", "Diciembre", "Marzo", "Julio", "Noviembre"];
    let meses = array.includes("Enero");
    let mes = array.includes("Marzo");
    let x = array.includes("Noviembre");
    let anio = [meses && mes && x];
    if (x && mes && meses) {
        return "Meses Correctos";
    } else x && meses && mes;
    return "No se encuentra el mes pedido";
}
mesesDelAño();
console.log(mesesDelAño());

//La función recibe un array con enteros entre 0 y 200. Recorrer el array y guardar en un nuevo array sólo los
//valores mayores a 100 (no incluye el 100). Finalmente devolver el nuevo array.
// Tu código:

function mayorACien(array) {
    array = [0, 25, 50, 87, 100, 150, 180, 200]
    let numero = array.filter(element => element > 100)
    return numero
}
mayorACien();
console.log(mayorACien())


//Iterar en un bucle aumentando en 2 el numero recibido hasta un límite de 10 veces.
//Guardar cada nuevo valor en un array.
//Devolver el array
//Si en algún momento el valor de la suma y la cantidad de iteraciones coinciden, debe interrumpirse la ejecución y
//devolver: "Se interrumpió la ejecución"
//Pista: usá el statement 'break'
// Tu código:

function breakStatement(numero) {
    numero = 2;
    let suma = numero + 2;
    while (suma++) {
        if (suma <= 10) {
            break;
        }
    } return "Se interrumpió la ejecución"
}

breakStatement();
console.log(breakStatement())


//Iterar en un bucle aumentando en 2 el numero recibido hasta un límite de 10 veces.
//Guardar cada nuevo valor en un array.
//Devolver el array
//Cuando el número de iteraciones alcance el valor 5, no se suma en ese caso y se continua con la siguiente iteración
//Pista: usá el statement 'continue'
// Tu código:

function continueStatement(numero) {
     numero = 1;
    const array = [];
    for (let i = 0; i <= 10; i++) {
        numero += 2;
        if (i === 5) {
        numero = 0;
            continue;
        }
        
        array.push(numero);
    } return array
}
continueStatement();
console.log(continueStatement())


// No modificar nada debajo de esta línea
// --------------------------------

module.exports = {
    devolverPrimerElemento,
    devolverUltimoElemento,
    obtenerLargoDelArray,
    incrementarPorUno,
    agregarItemAlFinalDelArray,
    agregarItemAlComienzoDelArray,
    dePalabrasAFrase,
    arrayContiene,
    agregarNumeros,
    promedioResultadosTest,
    numeroMasGrande,
    multiplicarArgumentos,
    cuentoElementos,
    diaDeLaSemana,
    empiezaConNueve,
    todosIguales,
    mesesDelAño,
    mayorACien,
    breakStatement,
    continueStatement,
};
